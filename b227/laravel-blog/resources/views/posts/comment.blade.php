@extends('layouts.app')

@section('content')
<div class="modal" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Post a Comment</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      	<form>
      		<div class="form-group">
			<label for="content">Content:</label>
			<textarea class="form-control" if="content" name="content" rows="3"> </textarea> 
		</div></form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Post Comment</button>
      </div>
    </div>
  </div>
</div>

@endsection