{{-- <?php print_r($posts); ?> --}}

@extends('layouts.app')

@section('content')
    {{-- If there is already a post create it will display  --}}
    @if(count($posts) > 0)
        @foreach($posts as $post)
            <div class="card text-center mt-3">
                <div class="card-body">
                    <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                    <h6 class="card-text mb-3">Author: {{$post->user->name}}</h6>
                    {{-- eloquent have a specific functionality that automatically joins the table, once the relationship is identified in the Models. --}}
                    <p class="card-subtitle mb-3 text-muted">Created at: {{$post->created_at}}</p>
                </div>
                {{-- check if there is an authenticated user to prevent our web app from throwing an error when no user is logged in (not logged in users will have no access with the delete and edit button) --}}
                @if(Auth::user())
                    {{-- if the authenticated user is the author of this blog post (delete and edit button will only visible for users owned post.) --}}
                    @if(Auth::user()->id == $post->user_id)
                        {{-- show an edit post button and a delete post button --}}
                        <div class="card-footer">
                            <form method="POST" action="/posts/{{$post->id}}">
                                <a href="/posts/{{$post->id}}/edit" class="btn btn-primary">Edit post</a>
                                @if($post->isActive)
                                    {{-- 
                                        Method spoofing
                                            - HTML do not support "PUT", "PATCH", or "DELETE" 
                                            - using the @method() this will add a hidden _method field to a form and  will be used as the HTTP request method. 
                                    --}}
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Delete Post</button>
                                @else
                                    @method('PUT')
                                    @csrf
                                    <button type="submit" class="btn btn-secondary">Activate Post</button>
                                @endif
                            </form>
                        </div>
                    @endif
                @endif
            </div>
        @endforeach
    @else
        <div>
            <h2>There are no posts to show</h2>
            <a href="/posts/create" class="btn btn-info">Create post</a>
        </div>
    @endif
@endsection
